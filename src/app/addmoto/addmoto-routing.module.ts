import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddmotoPage } from './addmoto.page';

const routes: Routes = [
  {
    path: '',
    component: AddmotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddmotoPageRoutingModule {}
