import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addmoto',
  templateUrl: './addmoto.page.html',
  styleUrls: ['./addmoto.page.scss'],
})
export class AddmotoPage implements OnInit {

  marca : string ;
  modelo: string;
  year: string;
  precio: string;
  image: any;

  constructor(private router: Router) { }

  addMoto(){
    this.image = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    const formData = new FormData();
    formData.append('image', this.image);
    formData.append('marca', this.marca);
    formData.append('modelo', this.modelo);
    formData.append('year', this.year);
    formData.append('precio', this.precio);  
    
    let url = 'http://carlos-lopez-7e4.alwaysdata.net/motos';
    let options = {
      method: 'POST',
      body : formData
    }; 

    fetch(url, options)
      .then(response => this.router.navigate(['home']))
      .catch(err => console.error('error:' + err));
   
  }

  ngOnInit() {
  }

}
