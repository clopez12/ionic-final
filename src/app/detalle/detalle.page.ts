import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {
  data : any;
  idMoto : String ;
  constructor(private route: ActivatedRoute, private router: Router) { 
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.any;
        this.idMoto = this.data.id;
        console.log(this.data);
      }
    });
  
  }

  deleteMoto() { 
    const url = "http://carlos-lopez-7e4.alwaysdata.net/motos/" + this.idMoto;
    fetch(url, {
      "method": "DELETE"
    })
    .then(response => {
        this.router.navigateByUrl('/home');
      });
  }

  ngOnInit() {
  }

  

}
