import { ThrowStmt } from '@angular/compiler';
import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  motos;
  apiUrl = 'http://carlos-lopez-7e4.alwaysdata.net';

  constructor(private menu:MenuController, private router: Router) {
    this.getJson('todas');
    this.openFirst();
  }

  async getJson(filtro:String){
    if(filtro=='ducati'){
      const respuesta = await fetch(this.apiUrl+"/motos?marca=Ducati");
      this.motos = await respuesta.json();    
    }else if(filtro=='honda'){
      const respuesta = await fetch(this.apiUrl+"/motos?marca=Honda");
      this.motos = await respuesta.json();    
    }else if(filtro=='yamaha'){
      const respuesta = await fetch(this.apiUrl+"/motos?marca=Yamaha");
      this.motos = await respuesta.json();    
    }else if(filtro=='todas'){
      const respuesta = await fetch(this.apiUrl+"/motos");
      this.motos = await respuesta.json();
    }
    this.menu.close('first');
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  pasoParametro(index:number){
     var currentMoto: any = this.motos[index];
     let navigationExtras: NavigationExtras = {
       state: {
         any:currentMoto,
       }
     }
     this.router.navigate(['detalle'], navigationExtras);
  }

  ionViewWillEnter(){
    this.getJson('todas');
  }
  
  addMoto(){
    this.router.navigate(['addmoto']);
  }
}